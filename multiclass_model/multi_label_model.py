from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras import backend as K

class SmallerVGGNet:
	@staticmethod
	def build(width, height, depth, classes, finalAct="softmax"):
		# modeli, giriş şeklinin yanı sıra
		# "channel_last" ve kanalların kendisi
		model = Sequential()
		inputShape = (height, width, depth)
		chanDim = -1

		# "channel_first" kullanıyorsak, giriş şeklini güncelle
		# ve kanal boyutu
		if K.image_data_format() == "channels_first":
			inputShape = (depth, height, width)
			chanDim = 1

		# CONV => RELU => POOL
		model.add(Conv2D(32, (3, 3), padding="same",
			input_shape=inputShape))
		model.add(Activation("relu"))
		model.add(BatchNormalization(axis=chanDim))
		model.add(MaxPooling2D(pool_size=(3, 3)))
		model.add(Dropout(0.25))
		"""
		CONV katmanımız 3 x 3 çekirdek ve RELU aktivasyonu (Rectified Linear Unit) ile 32 filtreye sahiptir. 
		Toplu normalleştirme, maksimum havuzlama ve% 25'lik düşüşü uygularız.
		Çıkarma, düğümleri geçerli katmandan bir sonraki katmana rasgele ayırma işlemidir.  
		Bu rastlantısal bağlantı kesilme işlemi, ağda belirli bir sınıfı, nesneyi, 
		kenarı veya köşeyi tahmin etmekten sorumlu olan tek bir düğümün olmadığı için ağın aşırı uyumu azaltmasına 
		doğal olarak yardımcı olur.
		"""
		# (CONV => RELU) * 2 => POOL
		model.add(Conv2D(64, (3, 3), padding="same"))
		model.add(Activation("relu"))
		model.add(BatchNormalization(axis=chanDim))
		model.add(Conv2D(64, (3, 3), padding="same"))
		model.add(Activation("relu"))
		model.add(BatchNormalization(axis=chanDim))
		model.add(MaxPooling2D(pool_size=(2, 2)))
		model.add(Dropout(0.25))
		"""
		bu da mekansal boyutu kademeli olarak azaltmak, ancak derinliği artırmak için birlikte çalışır.
		"""

		# (CONV => RELU) * 2 => POOL
		model.add(Conv2D(128, (3, 3), padding="same"))
		model.add(Activation("relu"))
		model.add(BatchNormalization(axis=chanDim))
		model.add(Conv2D(128, (3, 3), padding="same"))
		model.add(Activation("relu"))
		model.add(BatchNormalization(axis=chanDim))
		model.add(MaxPooling2D(pool_size=(2, 2)))
		model.add(Dropout(0.25))

		# FC => RELU layers
		model.add(Flatten())
		model.add(Dense(1024))
		model.add(Activation("relu"))
		model.add(BatchNormalization())
		model.add(Dropout(0.5))

		# softmax sınıflandırıcı
		model.add(Dense(classes))
		model.add(Activation(finalAct))
		"""
		Tam bağlı katmanlar ağın sonuna yerleştirilir
		
		"""
		# yapılandırılmış ağ mimarisini döndür
		
		return model